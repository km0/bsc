# A small temp homepage for blobshopcollective.org

Hello we are preparing a website! For our collective! See it at [blobshopcollective.org](blobshopcollective.org).

## Blob Shop Collective

The Blob Shop Collective gathers multi-disciplinary artists and designers based in Amsterdam and Rotterdam (NL) and operates internationally. Our activities find their roots in inter-dependent publishing practices and foster connections across diverse subjectivities, backgrounds, and disciplines.

Blob Shop Collective combines the notion of a _shop_ — that acknowledges and reacts to economic necessities within the independent publishing field — with the self-organized structure of a _collective_ that caters togetherness. It is a meeting point where transactions and exchanges of knowledges, skills and resources are circulating.

Our desire to investigate publishing through experiments with new and traditional
media, collective methodologies and DIY tools manifests in online and offline publications, as well as intimate gatherings, workshops and public moments.

As a publishing project in the making, we seek agency to build sustainable and self-sufficient practices. Motivated by an open source mentality, we want to explore alternative ways of living and working together and with others.

_Ål Nik, Chae-Young Kim, Emma Prato, Erica Gargaglione, Francesco Luzzana, Gersande Schellinx, Jian Haake, Kimberley Cosmilla, Miriam Schöb, Mitsi Chaida and Supisara Burapachaisri_

## Contacts

For new collaboration, or to host already existing Blob Shop Collective projects, please contact us at [info at blobshopcollective.org](mailto:info@blobshopcollective.org).
Check also our [IG](https://www.instagram.com/blobshopcollective)
